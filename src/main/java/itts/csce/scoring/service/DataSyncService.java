package itts.csce.scoring.service;

import itts.csce.scoring.dto.User;
import itts.csce.scoring.view.FoodLibListVo;
import itts.csce.scoring.view.FoodListVo;
import itts.csce.scoring.view.UserInfoVo;

public interface DataSyncService {
    public FoodLibListVo getFoodLib();

    public UserInfoVo getUserInfo();

    public FoodListVo getFoods();
}
