package itts.csce.scoring.service.Impl;

import itts.csce.scoring.dao.FoodDao;
import itts.csce.scoring.dao.FoodLibDao;
import itts.csce.scoring.dao.UserDao;
import itts.csce.scoring.dto.User;
import itts.csce.scoring.service.DataSyncService;
import itts.csce.scoring.view.DisplayResult;
import itts.csce.scoring.view.FoodLibListVo;
import itts.csce.scoring.view.FoodListVo;
import itts.csce.scoring.view.UserInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@Qualifier("itts.csce.scoring.service.DataSyncService")
public class DataSyncServiceImpl implements DataSyncService {
    @Autowired
    private FoodLibDao foodLibDao;

    @Autowired
    private FoodDao foodDao;

    @Autowired
    private UserDao usersDao;

    DisplayResult displayResult;


    @Override
    public FoodLibListVo getFoodLib() {
        FoodLibListVo foodLibListVo = new FoodLibListVo();
        foodLibListVo.setFoodLibs(foodLibDao.findAll());
        displayResult.DisplayResult(foodLibListVo);

        return foodLibListVo;
    }

    @Override
    public UserInfoVo getUserInfo() {
        UserInfoVo userInfoVo = new UserInfoVo();
        User user = usersDao.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());

        userInfoVo.setUser(user);

        displayResult.DisplayResult("get user information successful", userInfoVo);

        return userInfoVo;
    }

    @Override
    public FoodListVo getFoods() {
        FoodListVo foodListVo = new FoodListVo();
        User user = usersDao.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());

        foodListVo.setFoodList(foodDao.findByUserId(user.getId()));

        displayResult.DisplayResult("get meal list successful", foodListVo);

        return foodListVo;
    }
}
