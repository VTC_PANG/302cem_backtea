package itts.csce.scoring.service.Impl;

import itts.csce.scoring.dao.FoodDao;
import itts.csce.scoring.dao.UserDao;
import itts.csce.scoring.dto.Food;
import itts.csce.scoring.dto.User;
import itts.csce.scoring.service.DataUpdateService;
import itts.csce.scoring.view.*;
import itts.csce.scoring.view.jwtauth.JwtResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Qualifier("itts.csce.scoring.service.DataUpdateService")
public class DataUpdateServiceImpl implements DataUpdateService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    UserDao usersDao;

    @Autowired
    FoodDao foodDao;

    DisplayResult displayResult;

    @Override
    public void registerAccount(UserVo userVo) {
        User newUser = new User();
        newUser.setUsername(userVo.getUsername());
        newUser.setPassword(passwordEncoder.encode(userVo.getPassword()));
        newUser.setAge(userVo.getAge());
        newUser.setHeight(userVo.getHeight());
        newUser.setWeight(userVo.getWeight());
        newUser.setGender(userVo.getGender());
        newUser.setPhone(userVo.getPhone());
        double bmi;
        double height = userVo.getHeight()/100;
        bmi = userVo.getWeight()/Math.pow(height,2);
        newUser.setBmi(Double.parseDouble(String.format("%.1f",bmi)));
        double bmr;

        if (userVo.getGender().equals("M")){
            bmr = 66 + (13.7 * userVo.getWeight()) + (5.0 * userVo.getHeight()) - (6.8 * userVo.getAge());
        }else{
            bmr = 655 + (9.6 * userVo.getWeight()) + (1.8 * userVo.getHeight()) - (4.7 * userVo.getAge());
        }
        newUser.setBmr(Double.parseDouble(String.format("%.1f",bmr)));


        usersDao.save(newUser);
        displayResult.DisplayResult(newUser+" register account successful!");

    }

    @Override
    public ResponseEntity<ResponseVo<JwtResponse>> checkUsername(UsernameVo username) {
        User currentUser = usersDao.findByUsername(username.getUsername());
        ResponseVo<JwtResponse> response = new ResponseVo<JwtResponse>();

        System.out.println(username.getUsername());
        if(currentUser!=null){
            System.out.println("can not use");
            displayResult.DisplayResult("Username already exist");
            return response.fail("400","Username already exist");
        }else {
            displayResult.DisplayResult("Username can be use");
            return response.success("Username can be use");
        }
    }

    @Override
    public void editUserInfo(EditUserInfoVo editUserInfoVo) {
        User user = usersDao.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());

        user.setAge(editUserInfoVo.getAge());
        user.setHeight(editUserInfoVo.getHeight());
        user.setWeight(editUserInfoVo.getWeight());
        user.setGender(editUserInfoVo.getGender());
        double bmi;
        double height = editUserInfoVo.getHeight()/100;
        bmi = editUserInfoVo.getWeight()/Math.pow(height,2);
        user.setBmi(Double.parseDouble(String.format("%.1f",bmi)));
        double bmr;

        if (editUserInfoVo.getGender().equals("M")){
            bmr = 66 + (13.7 * editUserInfoVo.getWeight()) + (5.0 * editUserInfoVo.getHeight()) - (6.8 * editUserInfoVo.getAge());
        }else{
            bmr = 655 + (9.6 * editUserInfoVo.getWeight()) + (1.8 * editUserInfoVo.getHeight()) - (4.7 * editUserInfoVo.getAge());
        }
        user.setBmr(Double.parseDouble(String.format("%.1f",bmr)));

        usersDao.save(user);

        displayResult.DisplayResult(user.getUsername()+" edit user information successful");

    }

    @Override
    public ResponseEntity<ResponseVo<JwtResponse>> uploadFood(FoodVo foodVo) {
        User user = usersDao.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());

        Food food = new Food();

        food.setFood_name(foodVo.getFood_name());
        food.setCalories(foodVo.getCalories());
        food.setFood_type(foodVo.getFood_type());
        food.setDate(foodVo.getDate());
        food.setMeal_type(foodVo.getMeal_type());
        food.setPortion(foodVo.getPortion());
        food.setUser_id(user.getId());

        foodDao.save(food);

        Food newFood = foodDao.findByFoodName(foodVo.getFood_name(),foodVo.getMeal_type(),user.getId(),foodVo.getDate());
        displayResult.DisplayResult(user.getUsername()+" add meal successful");
        ResponseVo<JwtResponse> response = new ResponseVo<JwtResponse>();

        return response.food_success(newFood.getFood_id());

    }
}
