package itts.csce.scoring.service;

import itts.csce.scoring.view.*;
import itts.csce.scoring.view.jwtauth.JwtResponse;
import org.springframework.http.ResponseEntity;

public interface DataUpdateService {
    public void registerAccount(UserVo userVo);

    public ResponseEntity<ResponseVo<JwtResponse>> checkUsername(UsernameVo username);

    public void editUserInfo(EditUserInfoVo editUserInfoVo);

    public ResponseEntity<ResponseVo<JwtResponse>> uploadFood(FoodVo foodVo);
}
