package itts.csce.scoring.service.jwtauth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import itts.csce.scoring.dao.UserDao;
import itts.csce.scoring.dto.User;

@Service
@Qualifier("itts.csce.scoring.service.jwtauth.JwtUserDetailsService")
public class JwtUserDetailsService implements UserDetailsService {

	@Autowired
	UserDao usersDao;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User currentUser = usersDao.findByUsername(username);
		System.out.println("username "+currentUser.getUsername()+"\npassword:"+currentUser.getPassword());
		UserBuilder builder = org.springframework.security.core.userdetails.User.withUsername(username);
		builder.password(currentUser.getPassword());
		builder.roles("role");
		return builder.build();
	}
}
