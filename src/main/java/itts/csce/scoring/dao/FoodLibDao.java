package itts.csce.scoring.dao;

import itts.csce.scoring.dto.FoodLib;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodLibDao extends JpaRepository<FoodLib, Integer> {
}
