package itts.csce.scoring.dao;

import itts.csce.scoring.dto.Food;
import itts.csce.scoring.dto.FoodLib;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface FoodDao extends JpaRepository<Food, Integer> {
    @Query("FROM Food f WHERE f.food_name = ?1 AND f.meal_type = ?2 AND f.user_id = ?3 AND f.date = ?4")
    public Food findByFoodName(String food_name, String meal_type, int user_id, Date date);

    @Query("FROM Food f WHERE f.user_id = ?1")
    public List<Food> findByUserId(int id);
}
