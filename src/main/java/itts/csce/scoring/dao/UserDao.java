package itts.csce.scoring.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import itts.csce.scoring.dto.User;

public interface UserDao extends JpaRepository<User, Integer> {
	User findByUsername(String username);


}