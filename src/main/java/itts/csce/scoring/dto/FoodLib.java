package itts.csce.scoring.dto;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name= "foodlib")
public class FoodLib implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int foodlib_id;
    private String type;
    private String subtype;
    private String food_name;
    private int calories;
    private int portion;

    public int getFoodlib_id() {
        return foodlib_id;
    }

    public void setFoodlib_id(int foodlib_id) {
        this.foodlib_id = foodlib_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getFood_name() {
        return food_name;
    }

    public void setFood_name(String food_name) {
        this.food_name = food_name;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public int getPortion() {
        return portion;
    }

    public void setPortion(int portion) {
        this.portion = portion;
    }
}
