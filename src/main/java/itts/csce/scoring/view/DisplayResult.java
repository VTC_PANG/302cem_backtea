package itts.csce.scoring.view;

import itts.csce.scoring.dao.UserDao;
import itts.csce.scoring.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

public class DisplayResult<T> {

    @Autowired
    UserDao usersDao;

    public void DisplayResult(String message){
        System.out.println(message);
    }



    public void DisplayResult(String message,T obj){
        User user = usersDao.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());

        System.out.println(user.getUsername() + message + "\n" +obj);
    }

    public void DisplayResult(T obj){
        System.out.println(obj);
    }
}
