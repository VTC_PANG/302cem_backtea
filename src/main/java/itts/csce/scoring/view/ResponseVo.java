package itts.csce.scoring.view;

import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.time.LocalDateTime;

public class ResponseVo<T> {
	private String status;
	private String code;

	@JsonInclude(Include.NON_NULL)
	private LocalDateTime currentTime;

	@JsonInclude(Include.NON_NULL)
	private String message;

	@JsonInclude(Include.NON_NULL)
	private T data;

	@JsonInclude(Include.NON_DEFAULT)
	private int food_id;

	public ResponseVo() {
		this.status = "success";
		this.code = "200";
	}

	public ResponseEntity<ResponseVo<T>> success(LocalDateTime currentTime, T obj) {
		this.data = obj;
		this.currentTime = currentTime;
		return ResponseEntity.ok(this);
	}

	public ResponseEntity<ResponseVo<T>> success() {
		return ResponseEntity.ok(this);
	}

	public ResponseEntity<ResponseVo<T>> success(T obj) {
		this.data = obj;
		return ResponseEntity.ok(this);
	}

	public ResponseEntity<ResponseVo<T>> success(String message, T obj) {
		this.message = message;
		this.data = obj;
		return ResponseEntity.ok(this);
	}

	public ResponseEntity<ResponseVo<T>> success(String message) {
		this.message = message;
		return ResponseEntity.ok(this);
	}

	public ResponseEntity<ResponseVo<T>> food_success(int id) {
		this.food_id = id;
		return ResponseEntity.ok(this);
	}

	public ResponseEntity<ResponseVo<T>> fail(String code, String message) {
		this.status = "fail";
		this.message = message;
		this.code = code;
		return ResponseEntity.badRequest().body(this);
	}

	public String getStatus() {
		return status;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public T getData() {
		return data;
	}

	public LocalDateTime getCurrentTime() {
		return currentTime;
	}

	public int getFood_id() {
		return food_id;
	}
}