package itts.csce.scoring.view;

import com.fasterxml.jackson.annotation.JsonIgnore;
import itts.csce.scoring.dto.User;

public class UserInfoVo {
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
