package itts.csce.scoring.view;

import itts.csce.scoring.dto.Food;

import java.util.List;

public class FoodListVo {
    private List<Food> foodList;


    public List<Food> getFoodList() {
        return foodList;
    }

    public void setFoodList(List<Food> foodList) {
        this.foodList = foodList;
    }
}
