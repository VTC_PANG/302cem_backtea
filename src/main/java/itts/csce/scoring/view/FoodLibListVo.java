package itts.csce.scoring.view;

import itts.csce.scoring.dto.FoodLib;

import java.util.List;

public class FoodLibListVo {
    private List<FoodLib> foodLibs;

    public List<FoodLib> getFoodLibs() {
        return foodLibs;
    }

    public void setFoodLibs(List<FoodLib> foodLibs) {
        this.foodLibs = foodLibs;
    }
}
