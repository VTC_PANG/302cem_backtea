package itts.csce.scoring.view.jwtauth;

import java.io.Serializable;

public class JwtResponse implements Serializable {
	private static final long serialVersionUID = 8683694654789624442L;
	private final String jwttoken;

	public JwtResponse(String jwttoken) {
		this.jwttoken = jwttoken;
	}

	public String getToken() {
		return this.jwttoken;
	}
}