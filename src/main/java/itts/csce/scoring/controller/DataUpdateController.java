package itts.csce.scoring.controller;

import itts.csce.scoring.service.DataUpdateService;
import itts.csce.scoring.view.*;
import itts.csce.scoring.view.jwtauth.JwtResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/update")
public class DataUpdateController {
    @Autowired
    @Qualifier("itts.csce.scoring.service.DataUpdateService")
    private DataUpdateService dataUpdateService;

    @PostMapping("/register")
    public ResponseEntity<ResponseVo<String>> registerAccount(@RequestBody UserVo userVo){
        dataUpdateService.registerAccount(userVo);
        return new ResponseVo<String>().success();
    }

    @PostMapping("/checkUsername")
    public ResponseEntity<ResponseVo<JwtResponse>> checkUsername(@RequestBody UsernameVo username){
        return dataUpdateService.checkUsername(username);
    }

    @PostMapping("/editUserInfo")
    public ResponseEntity<ResponseVo<String>> editUserInfo(@RequestBody EditUserInfoVo editUserInfoVo){
        dataUpdateService.editUserInfo(editUserInfoVo);
        return new ResponseVo<String>().success();
    }

    @PostMapping("/uploadFood")
    public ResponseEntity<ResponseVo<JwtResponse>> editUserInfo(@RequestBody FoodVo foodVo){
        return dataUpdateService.uploadFood(foodVo);
    }
}
