package itts.csce.scoring.controller.error;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import itts.csce.scoring.view.ResponseVo;

@RestController
public class CustErrController implements ErrorController {

	@Override
	public String getErrorPath() {
		// TODO Auto-generated method stub
		return "/error";
	}

	@RequestMapping("/error")
	public ResponseEntity<ResponseVo<String>> getError(HttpServletRequest request) {
		Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
		String message = (String) request.getAttribute("javax.servlet.error.message");
		Exception exception = (Exception) request.getAttribute("javax.servlet.error.exception");

		ResponseVo<String> response = new ResponseVo<String>();

		return response.fail(statusCode.toString(), exception == null ? message : message + ". " + exception.getMessage());
	}
}