package itts.csce.scoring.controller;

import itts.csce.scoring.dto.User;
import itts.csce.scoring.service.DataSyncService;
import itts.csce.scoring.service.DataUpdateService;
import itts.csce.scoring.view.FoodLibListVo;
import itts.csce.scoring.view.FoodListVo;
import itts.csce.scoring.view.ResponseVo;
import itts.csce.scoring.view.UserInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/sync")
public class DataSyncController {

    @Autowired
    @Qualifier("itts.csce.scoring.service.DataSyncService")
    private DataSyncService dataSyncService;

    @GetMapping("/foodlib")
    public ResponseEntity<ResponseVo<FoodLibListVo>> getFoolLib() {
        ResponseVo<FoodLibListVo> response = new ResponseVo<FoodLibListVo>();

        return response.success(LocalDateTime.now().withNano(0), dataSyncService.getFoodLib());
    }

    @GetMapping("/userinfo")
    public ResponseEntity<ResponseVo<UserInfoVo>> getUserinfo() {
        ResponseVo<UserInfoVo> response = new ResponseVo<UserInfoVo>();

        return response.success(LocalDateTime.now().withNano(0), dataSyncService.getUserInfo());
    }

    @GetMapping("/foodList")
    public ResponseEntity<ResponseVo<FoodListVo>> getFoodList() {
        ResponseVo<FoodListVo> response = new ResponseVo<FoodListVo>();

        return response.success(LocalDateTime.now().withNano(0), dataSyncService.getFoods());
    }
}
